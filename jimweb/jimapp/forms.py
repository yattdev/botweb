from django import forms
from django.contrib.auth import authenticate
from django.contrib.auth.forms import UserCreationForm
from .models import UserAccount


class TemporaryRegistrationForm(forms.Form):
    """
      Form for Temporary Registering new users with status 
    """
    email = forms.EmailField(
        # max_length=60,
        # help_text = 'Required. Add a valid email address',
        widget=forms.EmailInput(attrs={
            'type': 'email',
            'id': 'orangeForm-email',
            'class': 'form-control'
        }),
        required=True)
    class Meta:
        fields = ('email',)


class RegistrationForm(UserCreationForm):
    """
      Form for Registering new users with status 
    """
    email = forms.EmailField(
        max_length=60,
        help_text = 'Required. Add a valid email address',
        widget=forms.TextInput(attrs={
            'type': 'email',
            'id': 'orangeForm-email'
        }),
        required=True)
    username = forms.CharField(
        help_text = 'Required. Add a Username',
        widget=forms.TextInput(attrs={
            'type': 'text',
            'id': 'orangeForm-name'
            }),
        required=True)
    password = forms.CharField(
        widget=forms.PasswordInput(attrs={
            'type': 'password',
            'id': 'orangeForm-pass'
            }),
        required=True)
    class Meta:
        model = UserAccount
        fields = ('email', 'username', 'password', 'status')

    def __init__(self, *args, **kwargs):
        """
          specifying styles to fields 
        """
        super(RegistrationForm, self).__init__(*args, **kwargs)
        for field in (self.fields['email'], self.fields['username'],
                self.fields['password'], self.fields['status']):
            field.widget.attrs.update({'class': 'form-control '})


class UserAccountAuthenticationForm(forms.ModelForm):
    """
      Form for Logging in users
    """
    password  = forms.CharField(label= 'Password', widget=forms.PasswordInput)

    class Meta:
        model  =  UserAccount
        fields =  ('username', 'password')
        widgets = {
                   'username':forms.TextInput(attrs={'class':'form-control'}),
                   'password':forms.PasswordInput(attrs={'class':'form-control'}),
        }
    def __init__(self, *args, **kwargs):
        """
          specifying styles to fields 
        """
        super(UserAccountAuthenticationForm, self).__init__(*args, **kwargs)
        for field in (self.fields['username'], self.fields['password']):
            field.widget.attrs.update({'class': 'form-control '})

    def clean(self):
        if self.is_valid():
            # email = self.cleaned_data.get('email')
            password = self.cleaned_data.get('password')
            username = self.cleaned_data.get('username')
            if not authenticate(username=username, password=password):
                raise forms.ValidationError('Invalid Login')

class AccountUpdateform(forms.ModelForm):
    """
      Updating User Info
    """
    class Meta:
        model  = UserAccount
        fields = ('email', 'username', 'password')

    def __init__(self, *args, **kwargs):
        """
          specifying styles to fields
        """
        super(AccountUpdateform, self).__init__(*args, **kwargs)
        for field in (self.fields['email'],self.fields['username'],self.fields['password']):
            field.widget.attrs.update({'class': 'form-control '})

    def clean_email(self):
        if self.is_valid():
            email = self.cleaned_data['email']
            try:
                account = UserAccount.objects.exclude(pk = self.instance.pk).get(email=email)
            except UserAccount.DoesNotExist():
                return email
            raise forms.ValidationError("Email '%s' already in use." %email) # Traduire le msg en FR

    def clean_username(self):
        if self.is_valid():
            username = self.cleaned_data['username']
            try:
                account = UserAccount.objects.exclude(pk = self.instance.pk).get(username=username)
            except UserAccount.DoesNotExist:
                return username
            raise forms.ValidationError("Username '%s' already in use." % username) # Traduire le msg en FR
