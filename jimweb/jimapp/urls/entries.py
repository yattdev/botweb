"""Urls for the Zinnia entries"""
from django.conf.urls import url

from jimapp.views import CustomEntryDetail


urlpatterns = [
    url(r'^(?P<year>\d{4})/(?P<month>\d{2})/(?P<day>\d{2})/(?P<slug>[-\w]+)/$',
        CustomEntryDetail.as_view(),
        name='entry_detail'),
]
