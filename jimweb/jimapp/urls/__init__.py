"""Defaults urls for the Zinnia project"""
from django.conf.urls import include
from django.urls import re_path
from django.utils.translation import ugettext_lazy

from zinnia.settings import TRANSLATED_URLS


def i18n_url(re_path, translate=TRANSLATED_URLS):
    """
    Translate or not an re_path part.
    """
    if translate:
        return ugettext_lazy(re_path)
    return re_path


_ = i18n_url

urlpatterns = [
    re_path(r'^', include('jimapp.urls.zinnia_customs')),
]
