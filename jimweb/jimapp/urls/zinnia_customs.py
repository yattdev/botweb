#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django.conf.urls import include
from django.urls import re_path
from django.utils.translation import ugettext_lazy

from zinnia.settings import TRANSLATED_URLS


def i18n_url(re_path, translate=TRANSLATED_URLS):
    """
    Translate or not an re_path part.
    """
    if translate:
        return ugettext_lazy(re_path)
    return re_path


_ = i18n_url


app_name = 'zinnia'
urlpatterns = [
    re_path(_(r'^feeds/'), include('jimapp.urls.feeds')),
    re_path(_(r'^tags/'), include('jimapp.urls.tags')),
    re_path(_(r'^authors/'), include('jimapp.urls.authors')),
    re_path(_(r'^categories/'), include('jimapp.urls.categories')),
    re_path(_(r'^search/'), include('jimapp.urls.search')),
    re_path(_(r'^random/'), include('jimapp.urls.random')),
    re_path(_(r'^sitemap/'), include('jimapp.urls.sitemap')),
    re_path(_(r'^trackback/'), include('jimapp.urls.trackback')),
    re_path(_(r'^comments/'), include('jimapp.urls.comments')),
    re_path(r'^', include('jimapp.urls.entries')),
    re_path(r'^', include('jimapp.urls.archives')),
    re_path(r'^', include('jimapp.urls.shortlink')),
    re_path(r'^', include('jimapp.urls.quick_entry')),
    re_path(r'^', include('jimapp.urls.capabilities')),
]
