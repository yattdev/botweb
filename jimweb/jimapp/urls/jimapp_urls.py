"""jimapp URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  re_path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  re_path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, re_path
    2. Add a URL to urlpatterns:  re_path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import include, re_path, path
from django.conf import settings
from jimapp.views import (index, logout_view, registration_view, entry_list_view,
                   temporary_registration_view, CustomEntryInddex)
from django.utils.translation import ugettext_lazy

from zinnia.settings import TRANSLATED_URLS


#  TODO add translate urls to jimapp pages
def i18n_url(re_path, translate=TRANSLATED_URLS):
    """
    Translate or not an re_path part.
    """
    if translate:
        return ugettext_lazy(re_path)
    return re_path


_ = i18n_url


app_name = 'jimapp'
urlpatterns = [
    path('', index, name='home'),
    re_path(r'^home/', index, name='home'),
    re_path(r'^register/', registration_view, name='register'),
    re_path(r'^temporary_register/', temporary_registration_view,
        name='temporary_register'),
    # re_path('login/', login_view, name='login'),
    re_path(r'^logout/', logout_view, name='logout'),
]
