from django.db import models
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager
from ckeditor_uploader.fields import RichTextUploadingField
from ckeditor.fields import RichTextField
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.hashers import make_password
from zinnia.models_bases.entry import image_upload_to_dispatcher, AbstractEntry
#  from imagefield.fields import ImageField, PPOIField
from versatileimagefield.fields import VersatileImageField, PPOIField


# Create your models here.
class Status(models.Model):
    """
    docstring
    Cette represente le niveau des
    demarreur ou entrepreneur
    """
    title = models.CharField(max_length=200, unique=True)
    rang = models.FloatField(default=0.0)

    def __str__(self):
        return self.title


#manager for our custom user model
class UserAccountManager(BaseUserManager):
    """
        This is a manager for UserAccount class 
    """
    def create_user(self, email, username, password):
        if not email:
            raise ValueError("Users must have an Emaill address")
        if not password:
            raise ValueError("Users must have an Password")
        if not username:
            raise ValueError("Users must have an Username")
        user  = self.model(
                email=self.normalize_email(email),
                username=username,
                password=make_password(password)
            )

        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, username, password):
        user = self.create_user(
                email=self.normalize_email(email),
                username=username,
                password=make_password(password)
            )
        user.is_admin = True
        user.is_staff=True
        user.is_superuser=True
        user.save(using=self._db)
        return user


class UserAccount(AbstractBaseUser):
    """
      Custom user class inheriting AbstractBaseUser class 
    """
    email                = models.EmailField(verbose_name='email', max_length=60,
                                            unique=True, blank=False)
    username             = models.CharField(max_length=30, unique=True, blank=False)
    status               = models.ForeignKey(Status, on_delete=models.CASCADE,
                                            blank=True, null=True)
    is_temp              = models.BooleanField(default=False)
    date_joined          = models.DateTimeField(verbose_name='date joined', auto_now_add=True)
    last_login           = models.DateTimeField(verbose_name="last login", auto_now=True)
    is_admin             = models.BooleanField(default=False)
    is_active            = models.BooleanField(default=True)
    is_staff             = models.BooleanField(default=False)
    is_superuser         = models.BooleanField(default=False)

    REQUIRED_FIELDS = ['username', 'email', 'password']
    USERNAME_FIELD = 'email'

    objects = UserAccountManager()

    def __str__(self):
        return self.username
        
    def has_perm(self, perm, obj=None):
        return self.is_admin
    def has_module_perms(self, app_label ):
        return True


class Question(models.Model):
    """
    docstring
    Table des questions
    """
    title = RichTextField(verbose_name='Question', unique=True)
    slug = models.SlugField(blank=True, null=True)
    create_at = models.DateTimeField(auto_now_add=True)
    available = models.BooleanField(verbose_name="Cocher pour publier", default=False)
    rang = models.FloatField(default=0.0, verbose_name="Rang de la question")
    status = models.ManyToManyField(Status, related_name="questions", blank=False)

    def __str__(self):
        return "Question: {qsn}, Disponible: {dispo}, Rang: {rang}, \
        Status: {status}".format(qsn=self.title, dispo=self.available,
                                rang=self.rang, status=self.status)


class Answer(models.Model):
    """
    docstring
    Table des reponses.
    """
    content = RichTextUploadingField()
    create_at = models.DateTimeField(auto_now_add=True)
    available = models.BooleanField(verbose_name='Cocher pour publier' ,default=True)
    questions = models.ManyToManyField(Question, related_name="answers", blank=False)

    def __str__(self):
        return "Answer: "+ str(self.content)[0:50]


class Author(UserAccount):
    """
    docstring
    """
    name = models.CharField(verbose_name="Auteur",max_length=100, blank=False)
    answers = models.ManyToManyField(Answer, related_name="authors", blank=False)

    def __str__(self):
        return "Nom: {1}, Pseudo: {2}".format(self.name, self.username)


class Feedback(models.Model):
    """
    docstring
    """
    content = models.TextField(verbose_name="Commentaire")
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    answer = models.ForeignKey(Answer, on_delete=models.CASCADE)
    note = models.IntegerField(default=0)

    def __str__(self):
        return "Question: {qsn}, feedback: {fb}".format(qsn=self.question,
        fb=self.content)


class JimappAbstractEntry(AbstractEntry):

    """ JimappAbstractEntry override AbstractEntry 
	To customize some fields for my own needs
    """
    content = RichTextUploadingField(_('content'), blank=True)
    
    image = VersatileImageField(
        _('image'), blank=True,
        upload_to=image_upload_to_dispatcher,
        help_text=_('Used for illustration.'),
        width_field='width',
        height_field='height',
        ppoi_field='ppoi'
        )

    height = models.PositiveIntegerField(
        'Image Height',
        blank=True,
        null=True
    )
    width = models.PositiveIntegerField(
        'Image Width',
        blank=True,
        null=True
    )

    ppoi = PPOIField(
            'Image PPOI'
        )

    class Meta(AbstractEntry.Meta):
        abstract = True


