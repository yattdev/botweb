# Generated by Django 3.1.2 on 2020-11-05 13:01

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Answer',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('content', models.TextField(verbose_name='Reponse')),
                ('create_at', models.DateTimeField(auto_now_add=True)),
                ('available', models.BooleanField(default=True)),
            ],
        ),
        migrations.CreateModel(
            name='Status',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=200, unique=True)),
                ('rang', models.FloatField(default=0.0)),
            ],
        ),
        migrations.CreateModel(
            name='TemporaryUserAccount',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date_joined', models.DateTimeField(auto_now_add=True, verbose_name='date joined')),
                ('last_login', models.DateTimeField(auto_now=True, verbose_name='last login')),
                ('is_admin', models.BooleanField(default=False)),
                ('is_active', models.BooleanField(default=True)),
                ('is_staff', models.BooleanField(default=False)),
                ('is_superuser', models.BooleanField(default=False)),
                ('email', models.EmailField(max_length=60, unique=True, verbose_name='email')),
                ('password', models.CharField(blank=True, max_length=255, null=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='UserAccount',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('password', models.CharField(max_length=128, verbose_name='password')),
                ('date_joined', models.DateTimeField(auto_now_add=True, verbose_name='date joined')),
                ('last_login', models.DateTimeField(auto_now=True, verbose_name='last login')),
                ('is_admin', models.BooleanField(default=False)),
                ('is_active', models.BooleanField(default=True)),
                ('is_staff', models.BooleanField(default=False)),
                ('is_superuser', models.BooleanField(default=False)),
                ('email', models.EmailField(max_length=60, unique=True, verbose_name='email')),
                ('username', models.CharField(max_length=30, unique=True)),
                ('status', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='jimapp.status')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Question',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=200, unique=True, verbose_name='Question')),
                ('create_at', models.DateTimeField(auto_now_add=True)),
                ('available', models.BooleanField(default=True, verbose_name='Repondu')),
                ('rang', models.FloatField(default=0.0, verbose_name='Rang de la question')),
                ('status', models.ManyToManyField(related_name='questions', to='jimapp.Status')),
            ],
        ),
        migrations.CreateModel(
            name='Feedback',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('content', models.TextField(verbose_name='Commentaire')),
                ('note', models.IntegerField(default=0)),
                ('answer', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='jimapp.answer')),
                ('question', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='jimapp.question')),
            ],
        ),
        migrations.AddField(
            model_name='answer',
            name='questions',
            field=models.ManyToManyField(related_name='answers', to='jimapp.Question'),
        ),
        migrations.CreateModel(
            name='Author',
            fields=[
                ('useraccount_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='jimapp.useraccount')),
                ('name', models.CharField(max_length=100, verbose_name='Auteur')),
                ('answers', models.ManyToManyField(related_name='authors', to='jimapp.Answer')),
            ],
            options={
                'abstract': False,
            },
            bases=('jimapp.useraccount',),
        ),
    ]
