import random
import string
from django.utils.translation import get_language, activate, gettext_lazy as _
from django.shortcuts import resolve_url
from django.http import HttpResponse, HttpResponseRedirect
# from django.template import loader
from django.contrib import messages
from django.contrib.auth.hashers import make_password
from django.contrib.auth import (
                                  authenticate,
                                  logout ,
                                  login
                              )
from django.shortcuts import (
                                render,
                                get_object_or_404,
                                redirect
                              )
from .forms import (
                    RegistrationForm,
                    UserAccountAuthenticationForm,
                    TemporaryRegistrationForm
                    # AccountUpdateform
                )
from .models import (UserAccount, Question, Status, Answer,
                    Feedback, Author)
from zinnia.views.archives import EntryIndex
from zinnia.views.entries import EntryDetail


class CustomEntryInddex(EntryIndex):
    """ Custom EntryIndex, to custom the look of the articles 
        References: https://django-blog-zinnia.readthedocs.io/en/latest/how-to/
        customize_look_and_feel.html#changing-templates
    """
    template_name = 'jimapp/entry_list.html'


class CustomEntryDetail(EntryDetail):
    """ Custom DetailEntry to custom the look of the detil_article page
        References: https://django-blog-zinnia.readthedocs.io/en/latest/how-to/
        customize_look_and_feel.html#changing-templates
    """
    template_name = 'jimapp/entry_detail.html'    


def get_random_alphanumeric_string(length):
    """ Generate a random alphanumeric string of letters and digits """
    letters_and_digits = string.ascii_letters + string.digits
    result_str = ''.join((random.choice(letters_and_digits) for i in range(length)))
    return result_str

# Create your views here.
def entry_list_view(request):
    """ Renders post page """
    context = {}
    form = TemporaryRegistrationForm()
    context['form'] = form
    return render(request, "jimapp/entry_list.html", context)


# Create your views here.
def index(request):
    """ Renders Home page """
    context = {}
    form = TemporaryRegistrationForm()
    context['form'] = form
    trans = translate(language='fr')
    context['hero'] = trans
    return render(request, "jimapp/home.html", context)

def translate(language):
    cur_lang = get_language()
    try:
        activate(language)
        text = _('Build Your Landing Page With')
    finally:
        activate(cur_lang)
    return text


def temporary_registration_view(request):
    """
      Renders Temporary Registration Form 
    """
    context = {}
    if request.POST:
        form = TemporaryRegistrationForm(request.POST)
        if form.is_valid():
            email = form.cleaned_data['email']
            try:
                temp_user = UserAccount.objects.get(email=email)
            except UserAccount.DoesNotExist:
                """
                    Sign-in new user testeur
                    with random username and password
                """
                username = "TEMP_" + get_random_alphanumeric_string(10)
                password = get_random_alphanumeric_string(10)
                user_created = UserAccount.objects.create(email=email, username=username,
                password=make_password(password), is_temp=True)
                # """ Login Temporal User to App"""
                login(request, user_created)
                messages.success(request,
                "Bonne lecture ! {email}".format(email=email))
                return render(request, 'jimapp/question.html')
            else:
                if temp_user.is_temp:
                    """
                        The user already has an account, but it is temporary,
                        i.e. already tested app with his email, and must 
                        therefore register to continue using it
                    """
                    messages.info(request,
                    "Vous deja effectuer un teste, Completez votre inscription s'il vous plait !")
                    form_registration = RegistrationForm()
                    context = {
                        'form' : form,
                        'form_registration' : form_registration,
                    }
                    return render(request, "jimapp/base.html", context)
                else:
                    """ Utilisateur already have a count """
                    messages.info(request, "Vous avez deja un compte !")
                    form_auth = UserAccountAuthenticationForm()
                    context = {
                        'form' : form,
                        'form_auth': form_auth,
                    }
                    return render(request, "jimapp/base.html", context)
    else:
        form = TemporaryRegistrationForm()
        context['form'] = form
    return render(request, "jimapp/base.html", context)


def registration_view(request):
    """
      Renders Registration Form 
    """
    context = {}
    if request.POST:
        form = RegistrationForm(request.POST)
        if form.is_valid():
            user = form.save() #Save new User
            login(request, user)
            messages.success(request,
            "You have been Registereda as {}".format(request.user.username))
            return render(request, 'jimapp/question.html')
        else:
            messages.error(request, "Please Correct Below Errors")
            context['registration_form'] = form
    else:
        form = RegistrationForm()
        context['form'] = form
    return render(request, "jimapp/base.html", context)


def logout_view(request):
    """ Logout view """
    logout(request)
    messages.success(request, "Logged Out")
    return HttpResponseRedirect('/')

# def  login_view(request):
#     """
#       Renders Login Form
#     """
#     context = {}
#     user = request.user
#     if user.is_authenticated:
#         return redirect("home")
#     if request.POST:
#         form    = UserAccountAuthenticationForm(request.POST)
#         email   = request.POST.get('email')
#         password = request.POST.get('password')
#         user =  authenticate(email=email, password=password)
#         if user:
#             login(request, user)
#             messages.success(request, "Logged In")
#             return redirect("register")
#         else:
#             messages.error(request, "please Correct Below Errors")
#     else:
#         form = UserAccountAuthenticationForm()
#     context['login_form'] = form
#     return render(request, "accounts/login.html", context)


# def account_view (request):
#     """
#       Renders userprofile page "
#     """
#     if not request.user.is_authenticated:
#         return redirect("login")
#     context = {}
#     if request.POST:
#         form = AccountUpdateform(request.POST, instance = request.user)
#         if form.is_valid():
#             form.save()
#             messages.success(request, "profile Updated")
#         else:
#             messages.error(request, "Please Correct Below Errors")
#     else:
#         form  = AccountUpdateform(
#             initial={
#             'email':request.user.email,
#             'username':request.user.username,
#             }
#         )
#     context['account_form']=form

#     return render(request, "accounts/userprofile.html",context)
