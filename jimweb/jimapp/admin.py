from django.contrib import admin
from .models import (Status, Author, Answer, Feedback,
                    Question, UserAccount)
from django.utils.translation import ugettext_lazy as _

from zinnia.models.entry import Entry
from zinnia.admin.entry import EntryAdmin

class JimappEntryAdmin(EntryAdmin):
  fieldsets = (
    (_('Content'), {
      'fields': (('title', 'status'), 'lead', 'content',)}),
    (_('Illustration'), {
      'fields': ('image', 'width', 'height'),
      'classes': ('collapse', 'collapse-closed')}),) + \
    EntryAdmin.fieldsets[2:]

# Register your models here.
admin.site.register(Status)
admin.site.register(Question)
admin.site.register(Author)
admin.site.register(Answer)
admin.site.register(Feedback)
admin.site.register(UserAccount)
#  admin custom model for my entry
admin.site.register(Entry, JimappEntryAdmin)

