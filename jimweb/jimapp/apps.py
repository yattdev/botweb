from django.apps import AppConfig


class JimappConfig(AppConfig):
    name = 'jimapp'
