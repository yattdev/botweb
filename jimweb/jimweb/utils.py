#!/usr/bin/env python
# -*- coding: utf-8 -*-

# utils.py

def get_filename(filename, request):
    """ If you want to be able to have control over filename generation, you
        have to add a custom filename generator to your settings:
    """
    return filename.upper()
